FROM ubuntu:latest
MAINTAINER Zulhilmi Ramli <soul@outlook.my>

RUN apt-get update \
    && apt-get install -y \
        wget \
        openjdk-8-jre \
        xvfb \
        xz-utils \
    && apt-get clean

ENV PROCESSING_IDE_VERSION=3.3.7
RUN (wget -q -O- http://download.processing.org/processing-3.3.7-linux64.tgz \
    | tar -xz -C /usr/local/share \
    && ln -s /usr/local/share/processing-${PROCESSING_IDE_VERSION} /usr/local/share/processing \
    && ln -s /usr/local/share/processing-${PROCESSING_IDE_VERSION}/processing-java /usr/bin/processing-java)

CMD ["/bin/bash"]